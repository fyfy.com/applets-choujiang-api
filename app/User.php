<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $table = 'users';

    const UPDATED_AT = 'login_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'email','openid', 'password','avatar','sex','nickname','status','login_at','code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password','session_key'
    ];


    /**
     * passport默认使用email字段作为用户名，如果usename是其他字段必须重新定义findForPassport方法，
     * 且必须是public
     * @param $username
     * @return mixed
     */
    public function findForPassport($username)
    {
        return self::where('openid', $username)->first();
    }

    public function address(){
        return $this->hasOne('App\Models\Address','user_id','id');
    }


}
