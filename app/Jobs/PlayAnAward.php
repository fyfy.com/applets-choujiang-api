<?php

namespace App\Jobs;

use App\Http\Controllers\ActivityController;
use App\Models\Activity;
use App\Models\Join;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class PlayAnAward implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $activity;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Activity $activity)
    {
        //  ../static/add_bg.jpg

        $this->activity = $activity;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
         $this->playAnAward($this->activity['id']);

        //这里做任务的具体处理，可以用模型
//        Log::info('任务调度ss'.date('Y-m-d H:i:s',time()));
//        Log::info( $this->activity['id']);
    }


    /**
     * 开奖啦
     * @param $id
     * Author: DaXiong
     */
    protected function playAnAward($id)
    {
        try {
            $find = Activity::withCount(['join' => function ($query) use ($id) {
                $query->where('activity_id', $id);
            }])->find($id);
            if($find['status'] !== 1){
                return false;
            }
            $update = $find->update(['status' => 3, 'user_num' => $find['join_count']]);
            $join_count = $find['join_count'];
            if ($update and $join_count > 0) {
                $joinUser = Join::where('activity_id', $id)->get(['id', 'status'])->toArray();
                //如果参与人员>=一等奖设置人数
                $third_award = (int)$find['num1'] + (int)$find['num2'] + (int)$find['num3'];
                $second_award = (int)$find['num1'] + (int)$find['num2'];
                $first_award = (int)$find['num1'];





                if ($join_count >= $third_award) {
                    $winner_count = $third_award;
                }else{
                    $winner_count = $join_count;
                }
                $rand = $this->unique_rand(0, $join_count-1,$winner_count);
                for ($i = 0;$i<$join_count;$i++){
                    $joinUser[$i]['status'] = 4;
                }
                for ($i = 0;$i<$winner_count;$i++){
                    if($i < $first_award){
                        $joinUser[$rand[$i]]['status'] = 1;
                    }elseif($i < $second_award){
                        $joinUser[$rand[$i]]['status'] = 2;
                    }elseif($i < $third_award){
                        $joinUser[$rand[$i]]['status'] = 3;
                    }else{
                        $joinUser[$rand[$i]]['status'] = 4;
                    }
                }
                $joinModel = new Join();
                return $joinModel->updateBatch($joinUser);
            }
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }
        return false;
    }

    /**
     * array unique_rand( int $min, int $max, int $num )
     * 生成一定数量的不重复随机数
     * $min 和 $max: 指定随机数的范围
     * $num: 指定生成数量
     */

    public function unique_rand($min, $max, $num)
    {
        $count = 0;
        $return_arr = array();
        while ($count < $num) {
            $return_arr[] = mt_rand($min, $max);
            $return_arr = array_flip(array_flip($return_arr));
            $count = count($return_arr);
        }
        shuffle($return_arr);
        return $return_arr;
    }
}
