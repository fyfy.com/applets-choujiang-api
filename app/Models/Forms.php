<?php
/**
 * Created by PhpStorm.
 * Author: DaXiong
 * Date: 2018/11/8
 * Time: 13:52
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Forms extends Model
{
    protected $table = 'forms';
    //指定主键
    protected $primaryKey= 'form_id';
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = ['created_at','end_time'];
}