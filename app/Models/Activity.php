<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/10/16
 * Time: 15:11
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'activity';
    //public $timestamps = false;
    protected $guarded = [];

    const PAY_0 = 1; //是否付费：0未付1已付
    const PAY_1 = 2;

    const STATUS_1 = 1;//状态：1进行中2未通过3已结束
    const STATUS_2 = 2;
    const STATUS_3 = 3;
    const WINTYPE_0 = 0; //开奖类型：0时间，1人数，2手动
    const WINTYPE_1 = 1;
    const WINTYPE_2 = 2;

    const STATUS_TEXT = [
        self::STATUS_1 =>'<span style="color:#408BE3;">进行时</span>',
        self::STATUS_2 =>'<span style="color:#E34424;">已禁止</span>',
        self::STATUS_3 =>'<span style="color:#606060;">已结束</span>',
    ];

    protected $hidden = [
        'password'
    ];

    protected $dates = ['open_at'];

    public function user(){
        return $this->belongsTo('App\User');
    }
    public function join(){
        return $this->hasMany('App\Models\Join','activity_id','id');
    }


}