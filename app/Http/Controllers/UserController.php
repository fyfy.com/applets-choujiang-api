<?php
/**
 * Created by PhpStorm.
 * Author: DaXiong
 * Date: 2018/10/17
 * Time: 9:28
 */

namespace App\Http\Controllers;


use App\Models\Activity;
use App\Models\Join;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * 获取个人信息 get
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Author: DaXiong
     */
    public function info(Request $request){
        $user_id = auth()->guard('api')->id();
        $info = User::find($user_id);
        $info['join_count'] = Join::where('user_id',$user_id)->count();
        $info['join_win_count'] = Join::where('user_id',$user_id)->whereBetween('status',[1,3])->count();
        $info['activity_count'] = Activity::where('user_id',$user_id)->count();
        return $this->returnJson(0,'获取个人信息成功',$info);
    }

    /**
     * 我的-参与抽奖
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Author: DaXiong
     */
    public function join(Request $request){
        $user_id = auth()->guard('api')->id();
        $where['user_id'] = $user_id;
        $info = Join::where($where)->with('activity')
            ->orderBy('id','DESC')
            ->paginate(100);
        return $this->returnJson(0,'成功获取我参与抽奖信息',$info);
    }

    /**
     * 我的-发起抽奖
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Author: DaXiong
     */
    public function create(){
        $user_id = auth()->guard('api')->id();
        $where['user_id'] = $user_id;
        $info = Activity::where($where)
            ->with(['join' => function ($query) use ($user_id) {
                $query->where(['user_id' => $user_id])->get();
            }])
            ->orderBy('id','DESC')
            ->paginate(100);
        return $this->returnJson(0,'成功获取我发起抽奖信息',$info);
    }

    /**
     * 我的-中将记录
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Author: DaXiong
     */
    public function win(){
        $user_id = auth()->guard('api')->id();
        $where['user_id'] = $user_id;

        $info = Join::where($where)
            ->whereBetween('status',[1,3])
            ->with('activity')
            ->orderBy('id','DESC')
            ->paginate(100);
        return $this->returnJson(0,'',$info);
    }


}