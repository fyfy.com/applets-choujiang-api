<?php
/**
 * Created by PhpStorm.
 * Author: DaXiong
 * Date: 2018/11/8
 * Time: 13:52
 */

namespace App\Http\Controllers;


use App\Models\Forms;
use Carbon\Carbon;

class FormsController extends Controller
{

    public function add(){
        $user_id = auth()->guard('api')->id();
        $openid = auth()->guard('api')->user()->openid;
        $form_id = request('form_id','');
//        if(!$form_id || strlen($form_id) !== 13){
//            return $this->returnJson(1,'error');
//        }
        $add['user_id'] = $user_id;
        $add['open_id'] = $openid;
        $add['form_id'] = $form_id;
        $add['end_time'] = Carbon::now()->addDays(7);
        $add['created_at'] = Carbon::now();
        try{
            Forms::create($add);
            return $this->returnJson(0);
        }catch (\Exception $e){
            return $this->returnJson(1,'error');
        }

    }
}