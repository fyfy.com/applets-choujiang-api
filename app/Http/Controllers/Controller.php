<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * 返回提示信息
     * @param int $code 0正常；1错误
     * @param string $msg 提示信息 default:success
     * @param array $data 数据
     * @return \Illuminate\Http\JsonResponse
     */
    protected function returnJson($code = 0,$msg = '',$data = []){
        $return = [
            'code' => $code,
            'msg'  => empty($msg) ? 'success' : $msg,
            'data' => $data
        ];
        return response()->json($return);
    }

}
