<?php
/**
 * Created by PhpStorm.
 * Author: DaXiong
 * Date: 2018/10/29
 * Time: 15:05
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Join;

class JoinController extends Controller
{

    /**
     * 活动参与列表
     * @return \Illuminate\Http\JsonResponse
     * Author: DaXiong
     */
    public function index(){
        $params = [
            'activity_id' => (int)request('id',''),
            'status' => (int)request('status','')
        ];
        //dd(params['activity_id']);
        $info = Join::where(function($query) use($params){
                if ($params['status']){
                    $query->where('status',$params['status']);
                }
                if ($params['activity_id']){
                    $query->where('activity_id',$params['activity_id']);
                }
            })
            ->with('user')
            ->orderBy('id','DESC')
            ->paginate(5);
        return $this->returnJson(0,'',$info);
    }
}