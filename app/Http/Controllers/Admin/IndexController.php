<?php
/**
 * Created by PhpStorm.
 * Author: DaXiong
 * Date: 2018/10/29
 * Time: 11:15
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Activity;

class IndexController extends  Controller
{
    /**
     * 状态转换
     * @param $data
     * @param array $map
     * @return mixed
     */
    protected function stateToText(&$data, $map = []){
        foreach ($data as $key => &$row) {
            foreach ($map as $col => $pair) {
                if (isset($row[$col]) && isset($pair[$row[$col]])) {
                    $text = $col;
                    $row[$text] = $pair[$row[$col]];
                }
            }
        }
        return $data;
    }
    /**
     * 首页管理
     * @return \Illuminate\Http\JsonResponse
     * Author: DaXiong
     */
    public function index(){
        $params = [
            'status' => (int)request('status',''),
            'pay' => (int)request('pay',''),
            'title1' => (string)request('title1',''),
        ];
        $info = Activity::where(function($query) use($params){
                if ($params['status']){
                    $query->where('status',$params['status']);
                }
                if ($params['pay']){
                    $query->where('pay',$params['pay']);
                }
                if ($params['title1']){
                    $query->where('title1','like','%'.$params['title1'].'%');
                }
            })
            ->with('user')
            ->withCount('join')
            ->orderBy('id','DESC')
            ->paginate(10);
//        $this->stateToText($info,[
//            'status' => Activity::STATUS_TEXT,
//        ]);
        //dd($info);
        return $this->returnJson(0,'',$info);
    }

    /**
     * 首页管理_禁止
     * @return \Illuminate\Http\JsonResponse
     * Author: DaXiong
     */
    public function lock(){
        $id = (int)request('id',0);
        try{
            $info = Activity::find($id);
            if(!$info){
                return $this->returnJson(1,'提交非法数据');
            }
            $update = $info->update(['status'=>Activity::STATUS_2]);
            if($update){
                return $this->returnJson(0);
            }else{
                return $this->returnJson(1);
            }
        }catch (\Exception $e) {
            return $this->returnJson(1, $e->getMessage());
        }

    }
}