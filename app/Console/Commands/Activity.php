<?php
/**
 * Created by PhpStorm.
 * Author: DaXiong
 * Date: 2018/11/2
 * Time: 9:01
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class Activity extends Command
{
    /**
     * The name and signature of the console command.
     *用来描述命令的名字与参数
     * @var string
     */
    protected $signature = 'plan:activity';

    /**
     * The console command description.
     *存储命令描述
     * @var string
     */
    protected $description = 'activity 测试开奖';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *执行命令
     * @return mixed
     */
    public function handle()
    {
        //这里做任务的具体处理，可以用模型
        Log::info('任务调度二'.date('Y-m-d H:i:s',time()));
    }
}