<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Commands\Activity::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')2018-11-01 18:58:41
        //          ->hourly();

//        $schedule->call(function (){
//            $startTime =(string)date('Y-m-d h:i',strtotime(time())).':00';
//            $endTime =(string)date('Y-m-d h:i',strtotime(time())).'59';
//
//            $dataList = DB::table('activity')->where('status',1)->whereBetween('open_at', [$startTime, $endTime])-get();
//        })->everyMinute();
//        $schedule->command('plan:activity')->everyMinute();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
