<?php
/**
 * Created by PhpStorm.
 * Author: DaXiong
 * Date: 2018/10/29
 * Time: 9:18
 */
Route::get('/',function (){
    return '小样，请不要偷看了1';
});

//不需要登录
Route::post('login',   'AuthController@login');//登录
Route::post('register',  'AuthController@register');//创建管理员新用户
Route::post('change_password',   'AuthController@changePassword');//创建管理员新用户
Route::post('refresh', 'AuthController@refresh');//刷新token
Route::get('logout', 'AuthController@logout');//退出登录
Route::get('captcha', 'AuthController@captcha');//获取验证码

//需要登录
Route::group(['middleware'=>'auth:api'], function() {
    //首页管理
    Route::group(['prefix' => 'index'], function(){
        Route::get ('/','IndexController@index');//首页管理_列表
        Route::get ('lock','IndexController@lock');//首页管理_禁止
    });
    //用户
    Route::group(['prefix' => 'user'], function(){
        Route::get ('/','UserController@index');//微信用户_列表
        Route::get ('info','UserController@info');//管理员用户信息
        Route::post ('update','UserController@update');//管理员用户信息
        Route::delete ('destroy','UserController@destroy');//管理员用户信息
    });

    Route::group(['prefix' => 'join'], function(){
        Route::get ('/','JoinController@index');//活动参与人员_列表
    });
});
