<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/',function (){
    return '小样，请不要偷看了1';
});

Route::any('/login',   'PassportController@login');
Route::get('/logout',   'PassportController@logout');
Route::post('/refresh', 'PassportController@refresh');//刷新token
////微信小程序-活动抽奖
Route::group(['prefix' => 'activity'], function(){
    Route::get   ('/','ActivityController@index');//首页列表
    Route::get   ('info','ActivityController@activityInfo');//活动详情
    Route::post  ('create','ActivityController@create');//发起活动
    Route::post  ('upload','ActivityController@upload');
});

//微信小程序-参与活动抽奖
Route::group(['prefix' => 'join'], function(){
    Route::get  ('users','JoinController@info');//参与人员信息
    Route::post  ('add','JoinController@add');//参加活动
});
//微信小程序-活动抽奖-用户信息
Route::group(['prefix' => 'user'], function(){
    Route::get   ('info','UserController@info');//用户个人信息
    Route::get   ('join','UserController@join');//我的-参与抽奖
    Route::get   ('create','UserController@create');//我的-发起抽奖
    Route::get   ('win','UserController@win');//我的-中将记录
});
//微信小程序-活动抽奖-用户地址信息
Route::group(['prefix' => 'address'], function(){
    Route::get   ('info','AddressController@info');//个人地址信息
    Route::post  ('update','AddressController@update');//更新用户地址
});


//Route::any  ('','WeChatController@customerService');//客服信息
//
//Route::group(['middleware' => 'auth:api'],function() {
//    Route::group(['prefix' => 'forms'], function(){
//        Route::post   ('add','FormsController@add');//收集formid
//    });
//
//    //微信小程序-活动抽奖
//    Route::group(['prefix' => 'activity'], function(){
//        Route::get   ('/','ActivityController@index');//首页列表
//        Route::get   ('info','ActivityController@activityInfo');//活动详情
//        Route::post  ('create','ActivityController@create');//发起活动
//        Route::post  ('upload','ActivityController@upload');//发起活动
//        Route::post  ('update','ActivityController@update');//更新/编辑活动信息
//        Route::post  ('lock','ActivityController@lock');//更新/编辑活动信息
//
//    });
//    //微信小程序-参与活动抽奖   http://applets-choujiang-api.bh/api/
//    Route::group(['prefix' => 'join'], function(){
//        Route::get  ('users','JoinController@info');//参与人员信息
//        Route::get  ('winner','JoinController@winner');//参与人员信息
//        Route::post  ('add','JoinController@add');//参加活动
//        Route::post  ('set_address','JoinController@setAddress');//参加活动
//    });
//    //微信小程序-活动抽奖-用户信息
//    Route::group(['prefix' => 'user'], function(){
//        Route::get   ('info','UserController@info');//用户个人信息
//        Route::get   ('join','UserController@join');//我的-参与抽奖
//        Route::get   ('create','UserController@create');//我的-发起抽奖
//        Route::get   ('win','UserController@win');//我的-中将记录
//    });
//    //微信小程序-活动抽奖-用户地址信息
//    Route::group(['prefix' => 'address'], function(){
//        Route::get   ('info','AddressController@info');//个人地址信息
//        Route::get   ('winuser','AddressController@winUserInfo');//个人地址信息
//        Route::post  ('update','AddressController@update');//更新用户地址
//    });
//
//});


//列表：index;更新：update；删除：destory；详情：info；激活：active；锁定：lock；所有;all；
//需要的依赖： possport laravelS(上线在使用) easywechat


