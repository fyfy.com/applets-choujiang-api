<?php
return [

    'proxy' => [
        'grant_type' => 'password',
        'client_id'  => env('OAUTH_CLIENT_ID'),
        'client_secret' => env('OAUTH_CLIENT_SECRET'),
        'scope'   => '*',
    ],

    'refresh' => [
        'grant_type' => 'refresh_token',
        'client_id'  => env('OAUTH_CLIENT_ID'),
        'client_secret' => env('OAUTH_CLIENT_SECRET'),
        'scope'   => '*',
    ],
];
